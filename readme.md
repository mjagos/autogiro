### AutoGiro
AutoGiro is a management system for Degiro investment broker. 
Its goal is to allow completely autonomous portfolio system by implementing features that are missing in Degiro's web interface.

### Features
- Auto-Buy and Automatic allocation of fund: implements automatic buying when there's enough resources in a **vault**. 
The system tracks artificial **vaults** for each stock and redistributes between the vaults everytime there are new resources in your account based on specified percentages.
By setting up a standing order + cron job that runs this feature you can achieve completely autonomous system. 

- Portfolio tracking: DEGIRO doesn't provide any information about portfolio performance over time. 
This feature stores SQLite database in your S3 bucket. A new record is inserted (or updated) every time the job is executed.
  
- Portfolio visualization: This feature uses the SQLite database created by the previous feature and generates simple graph in a public S3 bucket that can be accessed via web browser.

### Requirements
- Python 3.8+
- AWS account (you will be running this in your own AWS environment)

### Limitations:
- Only works with DEGIRO account that has 2FA disabled (probably won't change in the future OR degiro needs to release official API)
- Must be run inside AWS environment and only works for eu-central-1, this is because of AWS Layer with the package is region specific
- This is a work in progress personal project. The project is fully functional, tested and used, however the setup is not user-friendly and the code is not yet fully parametrized for general usage.

### Quickstart
```
from autogiro.AutoGiro import AutoGiro
import logging

DEGIRO_SECRET_ID = 'your_secret_id'
BUCKET_NAME = 'your_s3_bucket'
BUCKET_NAME_UPLOAD = 'your_public_s3_bucket'
SNS_ARN = 'your_failure_queue_arn'

def lambda_handler(event, context) -> list:
    logging.getLogger().setLevel(logging.INFO)
    autogiro = AutoGiro(BUCKET_NAME, BUCKET_NAME_UPLOAD, SNS_ARN, DEGIRO_SECRET_ID)
    resp = autogiro.portfolio_manager() # To execute Auto-Buy and Automatic allocation of fund
    resp = autogiro.portfolio_tracker() # To execute Tracking of portfolio performance
    resp = autogiro.portfolio_grapher() # To execute Creating a simple graph that can be accessed via web browser

    return resp
```

### Setting up AWS account
option 1: manually
   - Create S3 bucket called degiro-data and degiro-data-public
   - Create SNS topic called failure_queue that consumes failed messages from lambda and sends them to your email address
   - Create Secret in Secrets Manager with user and pwd keys and your DEGIRO credentials
   - Create Python 3.8 lambda function with the following Layer: arn:aws:lambda:eu-central-1:163373143930:layer:autogiro_packages:2
   - Inside this function you can call the code from Quickstart section
   - Schedule a EventBridge trigger for this lambda function (for example every working day at 8:00 AM) to achieve autonomous system
   - Put products.json into your private s3 bucket with the following structure:

```
{
   "5277189": { # degiro product id
     "name": "iShares NASDAQ 100 UCITS ETF USD (Acc)", # optional name
     "weight": 0.7, # percentage of portfolio to allocate
     "vault": 0, # vault, start with 0
     "fee": 0 # purchase fee, is used to prevent buying too small amounts
   },
   "846771": {
     "name": "iShares Core MSCI Europe UCITS ETF EUR (Acc)",
     "weight": 0.3,
     "vault": 0,
     "fee": 0
   }
}
```

option 2: setup_aws.py: 
   - TBD

### Author
Marek Jagoš
