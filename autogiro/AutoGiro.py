import datetime
import logging
import json
from typing import Callable

import pytz
from boto3 import client

from autogiro.utils.general import is_market_open, update_db, construct_graph
from autogiro.utils.degiro_api import (get_degiro_conn, cashfunds_available, redistribute, put_orders,
                              wait_order_execution, resolve_transactions, drop_orders, get_portfolio)
from autogiro.utils.aws_services import push_sns_message, read_s3_file, write_s3_file

PRODUCTS_KEY = 'data/products.json'
DATABASE_KEY = 'data/database.db'
TRANSACTIONS_KEY = 'data/transactions.json'
FIGURE_KEY = 'portfolio.png'
LOCAL_FIG_PATH = '/tmp/fig.png'
LOCAL_DB_PATH = '/tmp/database.db'
DB_TABLE = 'PORTFOLIO'


def _feature_wrapper(feature: Callable):
    def wrapper(self, *args, **kwargs):
        try:
            feature(self)
        except Exception as e:
            push_sns_message(self.sns_arn, str(e))
            raise
        finally:
            if feature.__name__ == 'portfolio_manager':
                drop_orders(self.degiro)

        return feature(self, *args, **kwargs)
    return wrapper


class AutoGiro:
    def __init__(self, secret_degiro_login: str, bucket_priv: str, bucket_pub: str, sns_arn: str):
        self.secret_degiro_login = secret_degiro_login
        self.bucket_priv = bucket_priv
        self.bucket_pub = bucket_pub
        self.sns_arn = sns_arn

        self.degiro = get_degiro_conn(secret_degiro_login)
        self.s3_client = client('s3')

    @_feature_wrapper
    def portfolio_manager(self) -> dict:
        timestamp = datetime.datetime.today().astimezone(pytz.timezone('Europe/Prague'))
        if not is_market_open(timestamp):
            logging.info(f'Euronext (XAMS) is closed, {timestamp}, exiting..')
            return read_s3_file(self.s3_client, self.bucket_priv, PRODUCTS_KEY, is_json=True)
        else:
            logging.info(f'Euronext (XAMS) is open, {timestamp}')

        products = read_s3_file(self.s3_client, self.bucket_priv, PRODUCTS_KEY, is_json=True)
        transactions = read_s3_file(self.s3_client, self.bucket_priv, TRANSACTIONS_KEY, is_json=True)
        cf = cashfunds_available(self.degiro, products)
        if cf != 0:
            logging.info(f'Cashfunds available: {cf} €')
            redistribute(products, cf)
        else:
            logging.info(f'No cashfunds to redistribute')

        if put_orders(self.degiro, products):
            is_executed = wait_order_execution(self.degiro)
            orders = self.degiro.transactions(datetime.datetime.today(), datetime.datetime.today())
            products, transactions, orders = resolve_transactions(products, transactions, orders)
            if not is_executed:
                raise Exception('Placed orders failed to execute')
        else:
            logging.info('No orders executed, exiting..')

        write_s3_file(self.s3_client, self.bucket_priv, PRODUCTS_KEY, obj=json.dumps(products, indent=2))
        write_s3_file(self.s3_client, self.bucket_priv, TRANSACTIONS_KEY, obj=json.dumps(transactions, indent=2))

        return products

    @_feature_wrapper
    def portfolio_tracker(self) -> list:
        read_s3_file(self.s3_client, self.bucket_priv, DATABASE_KEY, LOCAL_DB_PATH)
        portfolio = get_portfolio(self.degiro)
        update_db(portfolio, LOCAL_DB_PATH, DB_TABLE)
        write_s3_file(self.s3_client, self.bucket_priv, DATABASE_KEY, LOCAL_DB_PATH)

        return portfolio

    @_feature_wrapper
    def portfolio_grapher(self):
        if not self.bucket_pub:
            raise Exception('No public bucket specified, the graph cannot be uploaded')

        products = read_s3_file(self.s3_client, self.bucket_priv, PRODUCTS_KEY, is_json=True)
        read_s3_file(self.s3_client, self.bucket_priv, DATABASE_KEY, LOCAL_DB_PATH)
        construct_graph(LOCAL_DB_PATH, LOCAL_FIG_PATH, products)
        write_s3_file(self.s3_client, self.bucket_pub, FIGURE_KEY, LOCAL_FIG_PATH, ContentType="image/png")
