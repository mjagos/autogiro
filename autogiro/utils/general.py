import pytz
import datetime
from matplotlib import pyplot as plt
import sqlite3


def round_(n: float) -> float:
    return round(n, 2)


def is_market_open(timestamp: datetime.datetime) -> bool:
    is_market_time = timestamp.replace(hour=9, minute=5) < timestamp < timestamp.replace(hour=17, minute=25)
    is_market_day = datetime.date.today().weekday() < 5

    # removed in order to remove pandas dependency to decrease venv size to fit AWS Lambda
    # is_market_day = not mcal.get_calendar('XAMS').schedule(start_date=timestamp.strftime('%Y-%m-%d'),
    #                                                        end_date=timestamp.strftime('%Y-%m-%d')).empty

    return is_market_time and is_market_day


def update_db(portfolio: list, local_db_path: str, sqlite_table: str) -> None:
    date = datetime.datetime.today().astimezone(pytz.timezone('Europe/Prague')).strftime('%Y-%m-%d')

    with sqlite3.connect(local_db_path) as sql:
        sql_cur = sql.cursor()

        delete_query = f'DELETE FROM {sqlite_table} WHERE DATE = \'{date}\';'
        sql_cur.execute(delete_query)

        for stock_data in portfolio:
            _id = stock_data['id']
            position_type = stock_data["positionType"]
            size = stock_data["size"]
            price = stock_data["price"]
            value = stock_data["value"]
            breakeven = stock_data["breakEvenPrice"]

            insert_query = f'''
            INSERT INTO {sqlite_table} VALUES (
                '{_id}',
                '{position_type}',
                {size},
                {round_(price)},
                {round_(value)},
                {round_(breakeven)},
                '{date}'
            );
            '''
            sql_cur.execute(insert_query)

        sql.commit()


def construct_graph(local_db_path, local_fig_path, cfg):
    with sqlite3.connect(local_db_path) as sql:
        cur = sql.cursor()

        # Prepare figure
        plt.figure(figsize=(12, 8))
        plt.xlabel('Date')
        plt.ylabel('Value (€)')
        plt.title('Degiro Portfolio')
        plt.style.use('seaborn-v0_8-whitegrid')
        plt.grid(alpha=0.75, linewidth=0.75)

        # Portfolio value plot
        data = cur.execute('''SELECT DATE, SUM(VALUE) FROM PORTFOLIO GROUP BY DATE ORDER BY DATE ASC;''').fetchall()
        date, value = zip(*data)
        plt.plot(date, value, marker='o', markevery=[-1], label='Portfolio', color='royalblue')
        plt.xticks([date[0], date[len(date) // 4], date[2 * len(date) // 4], date[3 * len(date) // 4], date[-1]])

        # Individual stock plots
        stocks = cur.execute('''
           SELECT DISTINCT ID FROM PORTFOLIO WHERE positionType = 'PRODUCT' and DATE = (SELECT MAX(DATE) FROM PORTFOLIO);
           ''').fetchall()
        for _id in [s[0] for s in stocks]:
            data = cur.execute(f'''
               SELECT DATE, SUM(VALUE) FROM PORTFOLIO WHERE ID = '{_id}' GROUP BY DATE ORDER BY DATE ASC;
               ''').fetchall()
            date, value = zip(*data)
            plt.plot(date, value, alpha=0.4, label=cfg[str(_id)]['name'], linewidth=1)

        plt.legend(loc='upper left')
        plt.savefig(local_fig_path)
