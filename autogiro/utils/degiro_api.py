from typing import Union
import logging
import degiroapi
import time
import json
import datetime
from autogiro.utils.aws_services import read_secret
from autogiro.utils.general import round_


def get_degiro_conn(secret_degiro_login) -> degiroapi.DeGiro:
    resp = read_secret(secret_degiro_login)
    user, pwd = resp['user'], resp['pwd']
    degiro = degiroapi.DeGiro()
    degiro.login(user, pwd)
    logging.info('Degiro connection established')

    return degiro


def cashfunds_available(degiro: degiroapi.DeGiro, cfg: dict) -> float:
    vault = round_(sum([s['vault'] for s in cfg.values()]))
    funds = degiro.getdata(degiroapi.Data.Type.CASHFUNDS)
    funds = round_(float([f for f in funds if 'EUR' in f][0].split(' ')[1]))
    logging.info(f'degiro: {funds}, vault: {vault}')

    return round_(funds - vault)


def put_orders(degiro: degiroapi.DeGiro, cfg: dict) -> bool:
    any_execution = False
    for _id, v in cfg.items():
        price = degiro.real_time_price(_id, degiroapi.Interval.Type.One_Day)[0]['data']['lastPrice']
        shares = v['vault'] // price
        if shares > 0:
            time.sleep(1)
            degiro.buyorder(degiroapi.Order.Type.MARKET, _id, 1, shares)
            logging.info(f'Put order {v["name"]} {shares} x {price}')
            any_execution = True
        else:
            logging.info(f'No order for {v["name"]}, price {price}, vault: {v["vault"]}')

    return any_execution


def wait_order_execution(degiro: degiroapi.DeGiro) -> bool:
    start = time.time()

    while orders := degiro.orders(datetime.datetime.now() - datetime.timedelta(minutes=15),
                                  datetime.datetime.now(), True):
        if time.time() - start > 60:
            return False

        time.sleep(5)
        logging.info(f'open orders: {orders}')
        logging.info('waiting for orders to be executed, retrying in 5 seconds')

    return True


def resolve_transactions(products: dict, transactions: dict, orders: dict) -> Union[dict, dict, dict]:
    for o in orders:
        t_id = o['id']
        if str(t_id) in transactions.keys():  # Transaction was already resolved in different run
            continue
        if o['buysell'] == 'S' or o['productId'] == 1366266:  # 1366266 is code for EUR/CZK transaction
            continue

        logging.info(f'resolving order: {o}')
        prod_id = str(o['productId'])
        change = o['totalPlusAllFeesInBaseCurrency']

        products[prod_id]['vault'] = round_(products[prod_id]['vault'] + change)
        transactions[t_id] = o

    return products, transactions, orders


def drop_orders(degiro: degiroapi.DeGiro) -> None:
    orders = degiro.orders(datetime.datetime.now(), datetime.datetime.now(), True)
    for o in orders:
        logging.info(f'dropping order: {o}')
        degiro.delete_order(o['orderId'])
        time.sleep(1)


def get_portfolio(degiro: degiroapi.DeGiro) -> list:
    portfolio_data = degiro.getdata(degiroapi.Data.Type.PORTFOLIO, True)
    logging.info('live portfolio data retrieved:')
    logging.info(json.dumps(portfolio_data, indent=2))

    return portfolio_data


def redistribute(cfg: dict, cf: float) -> None:
    distributed = [round_(s['weight'] * cf) for s in cfg.values()]
    distributed[0] += round_(cf - sum(distributed))
    for i, val in enumerate(cfg.values()):
        logging.info(f'added {distributed[i]} to {val["name"]}')
        val['vault'] = round_(distributed[i] + val['vault'])

    logging.info('Funds redistributed to products file')
    logging.info(json.dumps(cfg, indent=2))
