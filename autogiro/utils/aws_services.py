from typing import Union
from boto3 import client, session
import json
import logging


def read_secret(secret_id: str) -> Union[str, str]:
    boto_session = session.Session()
    secrets_client = boto_session.client(
        service_name='secretsmanager',
        region_name='eu-central-1'
    )

    resp = secrets_client.get_secret_value(SecretId=secret_id)
    resp = json.loads(resp['SecretString'])

    return resp


def push_sns_message(sns_arn: str, message: str) -> None:
    sns = client("sns")
    resp = sns.publish(
        TargetArn=sns_arn,
        Message=message,
        Subject=f'AWS Lambda degiro_manager failure'
    )
    if resp['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise Exception('Failed to push message to SNS')

    logging.info('Message pushed to SNS queue')
    return resp


def read_s3_file(s3_client: client, bucket_name: str, key: str, local_path: str = None, is_json: bool = False) -> dict:
    if local_path:
        resp = s3_client.download_file(bucket_name, key, local_path)
    else:
        resp = s3_client.get_object(Bucket=bucket_name, Key=key)
        resp = resp['Body'].read().decode()

    logging.info(f'{bucket_name}/{key} retrieved from s3 {"into" + local_path if local_path else ""}')
    return json.loads(resp) if is_json else resp


def write_s3_file(s3_client: client, bucket_name: str, key: str, local_path: str = None, obj=None, **kwargs) -> None:
    if local_path:
        s3_client.upload_file(local_path, bucket_name, key, ExtraArgs=kwargs)
    else:
        resp = s3_client.put_object(Body=obj, Bucket=bucket_name, Key=key)
        if resp['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise Exception('Failed to write config to s3')

    logging.info(local_path + ' uploaded s3' if local_path else key + ' uploaded s3')

    return None
