from setuptools import setup, find_packages
  
setup( 
    name='autogiro', 
    version='1.0.0a1', 
    description='Degiro portfolio management system', 
    author='Marek Jagoš', 
    author_email='jagos.marek@outlook.cz', 
    packages=find_packages(), 
    install_requires=[ 
        'boto3',
        'pytz',
        'matplotlib',
        'requests'
    ], 
)